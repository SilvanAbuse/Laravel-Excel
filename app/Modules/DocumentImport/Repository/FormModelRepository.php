<?php

namespace App\Modules\DocumentImport\Repository;

use App\Modules\DocumentImport\Model\FormModel;

class FormModelRepository
{
    public function getHeaders(): array
    {
        return FormModel::pluck('name')->toArray();
    }

    public function getModelByFieldName(string $fieldName)
    {
        return FormModel::where('name', $fieldName)->first()->model;
    }

}
