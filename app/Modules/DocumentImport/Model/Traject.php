<?php

namespace App\Modules\DocumentImport\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Traject extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'status',
        'manager'
    ];
}
