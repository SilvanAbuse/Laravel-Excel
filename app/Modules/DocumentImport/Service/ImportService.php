<?php

namespace App\Modules\DocumentImport\Service;

use App\Modules\DocumentImport\Repository\FormModelRepository;
use Illuminate\Validation\Rule;

class ImportService
{
    public FormModelRepository $formModelRepository;


    public function __construct(FormModelRepository $formModelRepository)
    {
        $this->formModelRepository = $formModelRepository;
    }

    public function getValidationRules()
    {

        foreach ($this->formModelRepository->getHeaders() as $name) {
            $rules[strtolower($name)] = 'required';
        }

        $rules['status'] = ['required', Rule::exists('statuses', 'name')];
        return $rules;
    }


    public function getModelIdByCollumn(string $key, string $value): int
    {
        $statusModel = $this->formModelRepository->getModelByFieldName($key);

        return $statusModel::where('name', $value)->pluck('id')->first();
    }
}
