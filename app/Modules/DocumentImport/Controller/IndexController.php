<?php

namespace App\Modules\DocumentImport\Controller;

use App\Http\Controllers\Controller;
use App\Modules\DocumentImport\Import\TrajectsImport;
use App\Modules\DocumentImport\Model\Traject;
use App\Modules\DocumentImport\Repository\FormModelRepository;
use App\Modules\DocumentImport\Request\ApiDocumentImportRequest;
use App\Modules\DocumentImport\Request\WebDocumentImportRequest;
use App\Modules\DocumentImport\Service\ImportService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class IndexController extends Controller
{
    public $trajectsImport;

    public function __construct(TrajectsImport $trajectsImport)
    {
        $this->trajectsImport = app(TrajectsImport::class);
    }

    public function index()
    {
        return view('import');
    }

    public function import(WebDocumentImportRequest $request)
    {
        try {
            Excel::import($this->trajectsImport,  $request->file('file'));
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            return redirect()->back()->with('error', $e->errors());
        }
        return redirect()->back()->with('success', 'The success message!');
    }

    public function apiDocumentImport(ApiDocumentImportRequest $request)
    {
        try {
            Excel::import($this->trajectsImport, $request->file('file'));
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            return response()->json([
                'message' => $e->errors(),
            ]);
        }
        return response()->json([
            'message' => 'The success message!',
        ]);
    }

}
