<?php

namespace App\Modules\DocumentImport\Import;

use App\Modules\DocumentImport\Model\Traject;
use App\Modules\DocumentImport\Repository\FormModelRepository;
use App\Modules\DocumentImport\Service\ImportService;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;


class TrajectsImport implements ToModel, WithValidation, WithHeadingRow, WithBatchInserts
{
    use Importable;

    public FormModelRepository $formModelRepository;

    public ImportService $importService;

    public function __construct(FormModelRepository $formModelRepository, ImportService $importService)
    {
        $this->formModelRepository = $formModelRepository;
        $this->importService = $importService;
    }

    public function model(array $row)
    {
        return new Traject([
            'name' => $row['name'],
            'status' => $this->importService->getModelIdByCollumn('status', $row['status']),
            'manager' => $row['manager'],
        ]);
    }


    public function rules(): array
    {
        return $this->importService->getValidationRules();
    }

    public function batchSize(): int
    {
        return 100;
    }
}
