<?php

namespace App\Console\Commands;

use App\Modules\DocumentImport\Import\TrajectsImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class DocumentImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:document {--fileName=first.csv}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import from document into the database.';


    public $trajectsImport;

    public function __construct(TrajectsImport $trajectsImport)
    {
        parent::__construct();
        $this->trajectsImport = app(TrajectsImport::class);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Excel::import($this->trajectsImport, $this->option('fileName'));
    }
}
