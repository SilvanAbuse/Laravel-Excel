<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('form_models')->insert([
                'name' => 'name',
                'label' => 'Name Label',
                'type' => 'text',
                'model' => 'App\Modules\DocumentImport\Model\Traject',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        DB::table('form_models')->insert([
            'name' => 'status',
            'label' => 'Status Label',
            'type' => 'text',
            'model' => 'App\Modules\DocumentImport\Model\Status',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('form_models')->insert([
            'name' => 'manager',
            'label' => 'Manager Label',
            'type' => 'text',
            'model' => 'App\Modules\DocumentImport\Model\Traject',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

    }
}
