
## How to install

- ``composer install``
- ``php artisan migrate``
- ``php artisan db:seed``



## How to use

You have a few way to do it:

1. visit home page and upload your file.

2. You may use api endpoint to upload your file.

``
POST
``
/api/import

Form-Data parameter: ``file``




